<?php

use App\Http\Controllers\Encrypt\EncryptController;
use Illuminate\Support\Facades\Route;

Route::prefix('v1')->group(function (){

    Route::group(['prefix' => 'encrypt', 'middleware' => ['jwt']], function (){

        Route::get('me', [EncryptController::class, 'getUserKey']);

    });

});
