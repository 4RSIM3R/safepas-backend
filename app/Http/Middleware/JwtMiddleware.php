<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;


class JwtMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            $user = auth('api')->user();
            if (!$user) {
                return response()->json(['status' => 'Token Is Invalid'] , 400);
            }
        } catch (Exception $e) {
            if ($e instanceof TokenInvalidException) {
                return response()->json(['status' => 'Token is Invalid'] , 400);
            } else if ($e instanceof TokenExpiredException) {
                return response()->json(['status' => 'Token is Expired'] , 400);
            } else {
                return response()->json(['status' => 'Authorization Token not found'] , 400);
            }
        }

        return $next($request);
    }
}
