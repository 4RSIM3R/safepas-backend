<?php


namespace App\Http\Controllers\Auth;


use App\Http\Requests\AuthRequest;
use App\Http\Requests\RegisterRequest;
use Illuminate\Support\Facades\Validator;
use Repository\Auth\AuthRepository;
use Tymon\JWTAuth;
use Illuminate\Http\Request;
use Utils\WebResponseUtils;

class AuthController
{

    protected $authRepository;

    public function __construct(AuthRepository $authRepository)
    {
        $this->authRepository = $authRepository;
    }

    public function login(Request $request)
    {

        $validated = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:8'
        ]);

        if ($validated->fails()) {
            return WebResponseUtils::success($validated->errors(), 'validation error', 422);
        }

        $data = $validated->validated();

        return $this->authRepository->login($data['email'], $data['password']);
    }

    public function register(Request $request)
    {


        $validated = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:8',
            'name' => 'required'
        ]);

        if ($validated->fails()) {
            return WebResponseUtils::success($validated->errors(), 'validation error', 422);
        }

        $data = $validated->validated();

        return $this->authRepository->register($data['email'], $data['password'], $data['name']);
    }

}
