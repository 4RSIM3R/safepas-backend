<?php


namespace App\Http\Controllers\Encrypt;


use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Repository\Encrypt\EncryptRepository;
use Utils\WebResponseUtils;

class EncryptController extends Controller
{

    public function getUserKey(EncryptRepository $encryptRepository): JsonResponse
    {
       $result = $encryptRepository->getUserKey();
       return WebResponseUtils::success(['token' => $result], 'Success Getting Encrypt Token', 200);
    }

}
