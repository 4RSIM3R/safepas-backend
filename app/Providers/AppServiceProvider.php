<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Repository\Auth\AuthRepository;
use Repository\Auth\AuthRepositoryImpl;
use Repository\Encrypt\EncryptRepository;
use Repository\Encrypt\EncryptRepositoryImpl;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(AuthRepository::class, AuthRepositoryImpl::class);
        $this->app->bind(EncryptRepository::class, EncryptRepositoryImpl::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
