<?php


namespace Repository\Auth;


use App\Models\User;
use Aws\Kms\KmsClient;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Utils\RandomStringGenerator;
use Utils\WebResponseUtils;

class AuthRepositoryImpl implements AuthRepository
{

    public function login($email, $password): JsonResponse
    {

        $user = User::where('email', $email)->first();

        if (!$user || !Hash::check($password, $user->password)) {
            return WebResponseUtils::success('', 'The provided credential incorrect', 422);
        } else {
            $authAttempt = auth('api')->attempt([
                'email' => $email,
                'password' => $password,
            ]);
            return WebResponseUtils::success(['token' => $authAttempt], 'Login Success', 200);
        }

    }

    public function register($email, $password, $name): JsonResponse
    {

        DB::beginTransaction();

        try {

            $randString = RandomStringGenerator::generate();

            $kmsClient = new KmsClient([
                'profile' => 'default',
                'version' => 'latest',
                'region' => 'us-east-1'
            ]);

            $keyId = 'arn:aws:kms:us-east-1:694021469686:key/8c873493-2c5a-402c-965e-3f601b4e202f';

            $result = $kmsClient->encrypt([
                'KeyId' => $keyId,
                'Plaintext' => $randString,
            ]);

            $encodedKey = base64_encode($result->get('CiphertextBlob'));


            $user = User::create([
                'email' => $email,
                'password' => Hash::make($password),
                'name' => $name,
                'key' => $encodedKey,
            ]);

            DB::commit();
            return WebResponseUtils::success($user, 'Login success', 200);
        }catch (\Throwable $e) {
            DB::rollBack();
            return WebResponseUtils::success($e->getMessage(), 'Error', 500);
        }

    }


}
