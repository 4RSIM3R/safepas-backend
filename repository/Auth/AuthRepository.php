<?php


namespace Repository\Auth;


use Illuminate\Http\JsonResponse;

interface AuthRepository
{

    public function login($email, $password) : JsonResponse;
    public function register($email, $password, $name) : JsonResponse;

}
