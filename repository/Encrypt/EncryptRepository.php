<?php


namespace Repository\Encrypt;


interface EncryptRepository
{

    public function getUserKey() : string;

}
