<?php


namespace Repository\Encrypt;


use Aws\AwsClient;
use Aws\Kms\KmsClient;

class EncryptRepositoryImpl implements EncryptRepository
{

    public function getUserKey(): string
    {

        $user = auth('api')->user();

        $binaryKey = base64_decode($user->key);

        $kmsClient = new KmsClient([
            'profile' => 'default',
            'version' => 'latest',
            'region' => 'us-east-1'
        ]);

        $keyId = 'arn:aws:kms:us-east-1:694021469686:key/8c873493-2c5a-402c-965e-3f601b4e202f';

        $result = $kmsClient->decrypt([
            'CiphertextBlob' => $binaryKey,
        ]);

        return $result->get('Plaintext');

    }
}
