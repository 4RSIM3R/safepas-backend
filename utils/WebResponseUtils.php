<?php


namespace Utils;


use Illuminate\Http\JsonResponse;

class WebResponseUtils
{

    public static function success($data, $message, $statusCode = 200) : JsonResponse
    {
        return  response()->json([
            'message' => $message,
            'data' => $data
        ], $statusCode);
    }

}
