<?php


namespace Utils;


class RandomStringGenerator
{

    public static function generate($length = 7) : string
    {

        return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
    }

}
